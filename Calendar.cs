﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Financial
{
    public static class Calendar
    {
        public enum TimeStep
        {
            Daily,
            Weekly,
            Monthly,
            Quarterly,
            Halfyearly,
            Yearly
        }

        public static HashSet<DateTime> Holidays { get; private set; } = new HashSet<DateTime>();

        public static IEnumerable<DateTime> GenerateReportingDates(DateTime start, DateTime end, TimeStep step)
        {
            if (start > end) throw new ArgumentException("End date must fall later than start date.");
            //yield return start.Date;
            DateTime d = start.Date;
            switch (step)
            {
                case TimeStep.Daily:
                    while (d <= end)
                    {
                        yield return d;
                        d = d.AddDays(1);
                    }
                    break;
                case TimeStep.Weekly:
                    while (d.DayOfWeek != DayOfWeek.Sunday)
                    {
                        d = d.AddDays(1);
                    }
                    while (d <= end)
                    {
                        yield return d;
                        d = d.AddDays(7);
                    }
                    break;
                case TimeStep.Monthly:
                    d = new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month));
                    while (d <= end)
                    {
                        yield return d;
                        d = d.AddDays(1);
                        d = (new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month)));
                    }
                    break;
                case TimeStep.Quarterly:
                    d = new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month));
                    while (d <= end)
                    {
                        if (d.Month % 3 == 0)
                        {
                            yield return d;
                        }
                        d = d.AddDays(1);
                        d = (new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month)));
                    }
                    break;
                case TimeStep.Halfyearly:
                    d = new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month));
                    while (d <= end)
                    {
                        if (d.Month % 6 == 0)
                        {
                            yield return d;
                        }
                        d = d.AddDays(1);
                        d = (new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month)));
                    }
                    break;
                case TimeStep.Yearly:
                    d = new DateTime(d.Year, 12, 31);
                    while (d <= end)
                    {
                        yield return d;
                        d = d.AddYears(1);
                    }
                    break;
                default:
                    break;
            }
            //yield return end.Date;
        }

        public static DateTime WorkingDays(DateTime start, long days)
        {
            if (days == 0)
            {
                if (!IsWorkingDay(start)) throw new ArgumentException("Start date given is not a working day.");
                return start;
            }
            else if (days > 0)
            {
                while (days > 0)
                {
                    start = start.AddDays(1);
                    if (IsWorkingDay(start)) days--;
                }
                return start;
            }
            else
            {
                while (days < 0)
                {
                    start = start.AddDays(-1);
                    if (IsWorkingDay(start)) days++;
                }
                return start;
            }
        }

        public static bool IsWorkingDay(DateTime date)
        {
            return !Holidays.Contains(date) && date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday;
        }
    }
}

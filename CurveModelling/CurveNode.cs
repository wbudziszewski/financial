﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financial
{
    public class CurveNode
    {
        public double Maturity { get; private set; }

        public double Value { get; private set; }

        public double LiquidityScore { get; private set; }

        public double SanityScore { get; private set; }

        public double ProximityScore { get; private set; }

        public string Label { get { return String.Format("L{0:0.##} S{1:0.##} P{2:0.##}", LiquidityScore, SanityScore, ProximityScore); } }

        public double Score { get { return LiquidityScore * SanityScore * ProximityScore; } }

        public CurveNode(double maturity, double value, double liquidityScore, double sanityScore, double proximityScore)
        {
            Maturity = maturity;
            Value = value;
            LiquidityScore = liquidityScore;
            SanityScore = sanityScore;
            ProximityScore = proximityScore;
        }
    }
}

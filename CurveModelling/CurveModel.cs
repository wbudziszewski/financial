﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Financial
{
    internal abstract class CurveModel
    {
        protected internal List<CurveNode> Nodes { get; set; } = new List<CurveNode>();

        internal CurveModel()
        {
        }

        protected internal abstract void Recalculate();

        internal void AddNode(double maturity, double value)
        {
            Nodes.Add(new CurveNode(maturity, value, 1, 1, 1));
        }

        internal void SetNodes(IEnumerable<CurveNode> nodes)
        {
            Nodes.Clear();
            Nodes.AddRange(nodes);
        }

        protected internal abstract double Get(double t);

        protected internal IEnumerable<Point> Get(double tMin, double tMax, double tStep)
        {
            for (double i = tMin; i <= tMax; i += tStep)
            {
                yield return new Point(i, Get(i));
            }
        }
    }
}

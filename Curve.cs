﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Ink;
using System.Windows.Media;

namespace Financial
{
    public enum CurveModelType { NelsonSiegelSvensson, CubicSplines }

    public class Curve : INotifyPropertyChanged
    {
        CurveModel model;

        public double MinimumMaturity { get; set; } = 0;

        public double MaximumMaturity { get; set; } = 25;

        public string Name { get; set; }

        public Curve(CurveModelType type, string name)
        {
            Name = name;
            switch (type)
            {
                case CurveModelType.NelsonSiegelSvensson:
                    model = new NelsonSiegelSvenssonCurveModel();
                    break;
                case CurveModelType.CubicSplines:
                    model = new CubicSplinesCurveModel();
                    break;
                default:
                    break;
            }
        }

        public IEnumerable<Point> GetCurvePoints(double min, double max, double step)
        {
            return model.Get(min, max, step);
        }

        public IEnumerable<Point> GetNodesPoints()
        {
            foreach (var n in model.Nodes)
            {
                yield return new Point(n.Maturity, n.Value);
            }
        }

        public IEnumerable<CurveNode> GetNodes()
        {
            return model.Nodes.AsEnumerable();
        }

        public void AddNode(double maturity, double value)
        {
            model.AddNode(maturity, value);
            model.Recalculate();
        }

        public void SetNodes(IEnumerable<CurveNode> nodes)
        {
            model.SetNodes(nodes);
            model.Recalculate();
        }

        public double Get(double point)
        {
            return model.Get(point);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
